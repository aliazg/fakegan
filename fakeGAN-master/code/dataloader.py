import numpy as np

class Gen_Data_loader():
    def __init__(self, batch_size):                    
        self.batch_size = batch_size
        self.token_stream = []

    def create_batches(self, data):
        self.token_stream = data
        self.num_batch = int(len(self.token_stream) / self.batch_size)
        self.token_stream = self.token_stream[:self.num_batch * self.batch_size]
        self.sequence_batch = np.split(np.array(self.token_stream), self.num_batch, 0)         #将数据按行切分为self.num_batch份
        self.pointer = 0

    def next_batch(self):
        ret = self.sequence_batch[self.pointer]
        self.pointer = (self.pointer + 1) % self.num_batch
        return ret

    def reset_pointer(self):
        self.pointer = 0

class Dis_dataloader():
    def __init__(self, batch_size):
        self.batch_size = batch_size
        self.sentences = np.array([])
        self.labels = np.array([])

    def load_train_data(self, positive_examples, negative_examples):
        # Load data

        self.sentences = np.concatenate([positive_examples, negative_examples], 0)   #数组拼接   

        # Generate labels
        positive_labels = [[0, 1] for _ in positive_examples]
        negative_labels = [[1, 0] for _ in negative_examples]
        self.labels = np.concatenate([positive_labels, negative_labels], 0)

        # Shuffle the data
        shuffle_indices = np.random.permutation(np.arange(len(self.labels)))
        self.sentences = self.sentences[shuffle_indices]
        self.labels = self.labels[shuffle_indices]

        # Split batches
        self.num_batch = int(len(self.labels) / self.batch_size)
        self.sentences = self.sentences[:self.num_batch * self.batch_size]
        self.labels = self.labels[:self.num_batch * self.batch_size]
        self.sentences_batches = np.split(self.sentences, self.num_batch, 0)
        self.labels_batches = np.split(self.labels, self.num_batch, 0)

        self.pointer = 0


    def next_batch(self):
        ret = self.sentences_batches[self.pointer], self.labels_batches[self.pointer]
        self.pointer = (self.pointer + 1) % self.num_batch
        return ret

    def reset_pointer(self):
        self.pointer = 0

def read_embedding_vectors():
    vocab = []
    embd = []
    with open("glove.6B.200d.txt", "rb") as f:
        lines = f.readlines()
        for line in lines:
            tokens = line.split()
            vocab.append(tokens[0].decode('utf-8'))
            embd.append([np.float32(x) for x in tokens[1:]])
    return vocab, embd

def read_json(filepath):
    with open(filepath) as f:
        return json.load(f)
