import tensorflow as tf
import os
os.environ["CUDA_VISIBLE_DEVICES"]="0,1,2"

config = tf.ConfigProto()
config.gpu_options.allow_growth=True
sess = tf.Session(config=config)
keras.backend.set_session(sess)

def _get_available_devices():
    from tensorflow.python.client import device_lib
    local_device_protos = device_lib.list_local_devices()
    return [x.name for x in local_device_protos]

print (_get_available_devices())
